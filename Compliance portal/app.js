var express = require('express');
var session = require('express-session');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var dash = require('./routes/dash_board');
var nc = require('./routes/nc_unit');
var act= require('./routes/ncActwise');
var nc_All= require('./routes/nc_all');
var file_return= require('./routes/fileReturn');
var allVendor= require('./routes/vendors');
var act_return= require('./routes/act_return');

var app = express();

var checkSession = function(req, res, next) {
  var sess= req.session;
  if(sess.userName) {
      next();
    }
    else{
      res.redirect('/sessionExpired');
    }
}

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(session({secret: 'ssshhhhh', proxy: true, resave: true, saveUninitialized: true}));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/dash', dash);
app.use('/ncUnit',[checkSession],nc);
app.use('/actwise',[checkSession],act);
app.use('/ncAll',[checkSession],nc_All);
app.use('/fileReturn',[checkSession],file_return);
app.use('/vendors',[checkSession],allVendor);
app.use('/actReturn',[checkSession],act_return);
/*app.use('/browse',browsed);*/


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;
