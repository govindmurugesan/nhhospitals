var express = require('express');
var session = require('express-session');
var router = express.Router();
var request = require('request');
var db_url = require('../db/url');
var bodyParser = require('body-parser');
var sess;
/* GET users listing. */

var urlencodedParser = bodyParser.urlencoded({ extended: false })

router.post('/',urlencodedParser, function(req, res, next) {
	console.log('usr'+req.body.username);
	console.log('pass'+req.body.password);
	var user_name= req.body.username;
	var password= req.body.password;
 	var a = '';
 	db_url.db(a, function(err, url_db) {
	    if(err){
	    	console.log(err)
	    }else{
 			var dbUrl = url_db.url + url_db.login;
 			console.log(dbUrl);
			var options = { method: 'POST',
						  url: dbUrl,	 
						  body: { USR_NAME: user_name, PASS: password, IP: ''},
						  json: true 
			};

			request(options, function (error, response, body) {
				  if (error){
				  	res.redirect('/error')
				  }
				  else{
				  	console.log(body);
				  	sess = req.session;
					sess.userType = body.LoginResult[0].USR_TYPE;
					sess.userName = body.LoginResult[0].USR_NAME;
					/*in app.js page we should check (req.username== sess.userName) 
					otherwise corporate can also see the pages of user and vice versa
					by using /mapping of any page */
				  	if(sess.userType=='corporate'){
					  res.redirect('/dashboard')
					}else{
					  res.redirect('/fileReturn')
					}
				  }
			});
  		}
    });

});

module.exports = router;