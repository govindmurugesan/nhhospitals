var express = require('express');
var session = require('express-session');
var router = express.Router();
var request = require('request');
var db_url = require('../db/url');

var checkSession = function(req, res, next) {
	var sess= req.session;
	if(sess.userName) {
  		next();
  	}
  	else{
  		res.redirect('/sessionExpired');
  	}
}

router.get('/', function(req, res, next) {
  res.render('login');
});

router.get('/sessionExpired', function(req, res, next) {
	res.render('error',{error: "Session expired, please login to continue"});
});

router.get('/dashboard',[checkSession], function(req, res, next) {
	res.render('dashboard');
});

router.get('/error', function(req, res, next) {
 	res.render('error',{ error: 'Invalid username or password'});
});

router.get('/logout',function(req,res){
	req.session.destroy(function(err) {
	  if(err) {
	    console.log(err);
	  } else {
	    res.redirect('/');
	  }
	});
});
module.exports = router;