var express = require('express');
var session = require('express-session');
var router = express.Router();
var request = require('request');
var db_url = require('../db/url');

router.get('/:actName', function(req, res, next) {
	var a= "";
	var response_rout = res;
	var actname = req.params.actName;
	var sess = 	req.session;
	var userName = sess.userName;
	db_url.db(a, function(err, url_db) {
	    if(err){
	    	console.log(err)
	    }else{
	    	var dbUrl = url_db.url + url_db.act;
			var options = { method: 'POST',
						  url: dbUrl,	 
						  body: { USR_NAME: userName},
						  json: true 
			};

			request(options, function (error, response, body) {
				  if (error){
				  	res.redirect('/error')
				  }
				  else{

		 			var dbUrl = url_db.url + url_db.NCactWise;
		 			console.log(dbUrl);
					var options = { method: 'POST',
								  url: dbUrl,	 
								  body: { ACT: actname},
								  json: true 
					};

					request(options, function (error, response, NCactBody) {
						  if (error){
						  	res.redirect('/error')
						  }
						  else{
							response_rout.render('ncAct',{data:body.SelectAllActsResult, ncActdata:NCactBody.NonComplaintActWiseResult, ncActName: actname});
						  }
					});
				}
			});		
  		}
    });
 
});
module.exports = router;
