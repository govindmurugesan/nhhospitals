var express = require('express');
var session = require('express-session');
var router = express.Router();
var request = require('request');
var db_url = require('../db/url');

router.get('/', function(req, res, next) {
	var a= "";
	var response_rout= res;
	var sess = 	req.session;
	var userName = sess.userName;
	db_url.db(a, function(err, url_db) {
	    if(err){
	    	console.log(err)
	    }else{
	    	var dbUrl = url_db.url + url_db.all;
			var options = { method: 'POST',
						  url: dbUrl,	 
						  body: { USR_NAME: userName},
						  json: true 
			};

			request(options, function (error, response, all) {
				  if (error){
				  	res.redirect('/error')
				  }
				  else{response_rout.render('ncAll',{all_data:all.AllComplianceResult});	
					}
			});
		}

    });
  
});
module.exports = router;