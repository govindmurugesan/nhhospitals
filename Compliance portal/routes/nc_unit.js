var express = require('express');
var session = require('express-session');
var router = express.Router();
var request = require('request');
var db_url = require('../db/url');

router.get('/:unitName', function(req, res, next) {
	var a= "";
	var unitname = req.params.unitName;
	var response_rout= res;	
	var sess = req.session;
	var userName= sess.userName;
	console.log("getting login details");
	console.log(userName);
	db_url.db(a, function(err, url_db) {
	    if(err){
	    	console.log(err)
	    }else{
	    	var dbUrl = url_db.url + url_db.unit;
			var options = { method: 'POST',
						  url: dbUrl,	 
						  body: { USR_NAME: userName},
						  json: true 
			};

			request(options, function (error, response, unitbody) {
				  if (error){
				  	res.redirect('/error')
				  }
				  else{
				  		console.log(unitbody);
						var dbUrl = url_db.url + url_db.NCunitWise;
						var options = { method: 'POST',
									  url: dbUrl,	 
									  body: { UNIT: unitname},
									  json: true 
						};

						request(options, function (error, response, NCunitBody) {
							  if (error){
							  	res.redirect('/error')
							  }
							  else{
							  	console.log(NCunitBody);
								response_rout.render('ncUnit',{act_data:unitbody.SelectAllUnitsResult, ncUnitAct:NCunitBody.NonComplaintUnitWiseResult, ncUnitName: unitname});
							  }
						});
					}
			});
		}

    });
  
});
module.exports = router;