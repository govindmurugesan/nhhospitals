var express = require('express');
var session = require('express-session');
var router = express.Router();
var request = require('request');
var db_url = require('../db/url');
/* GET home page. */

router.get('/', function(req, res, next) {	
	var response_rout= res;
	var a = '';
	var sess= req.session;
	var userName= sess.userName;
 	db_url.db(a, function(err, url_db) {
	    if(err){
	    	console.log(err)
	    }else{
	    	console.log(url_db);
 			var dbUrl = url_db.url + url_db.act;
			var options = { method: 'POST',
						  url: dbUrl,	 
						  body: { USR_NAME: userName},
						  json: true 
			};

			request(options, function (error, response, body) {
				  if (error){
				  	res.redirect('/error')
				  }
				  else{
				  		var dbUrl = url_db.url + url_db.vendreq;
						var options = { method: 'POST',
									  url: dbUrl,	 
									  body: { USR_NAME: userName},
									  json: true 
						};

						request(options, function (error, response, vendor) {
							  if (error){
							  	res.redirect('/error')
							  }
							  else{
							  	response_rout.render('allVendor',{data:body.SelectAllActsResult, vendor_data:vendor.SelectAllVendorsResult});
							  }
						});
				  }
			});
  		}
    });
});
module.exports = router;